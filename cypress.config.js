const { defineConfig } = require('cypress')

module.exports = defineConfig({
  projectId: 'iu8zj1',

  e2e: {
    // Configure your E2E tests here
    specPattern: "cypress/e2e/**/*.{cy,spec}.{js,ts}"
  },
})